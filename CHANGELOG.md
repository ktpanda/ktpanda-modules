[Version 1.0.41 (2025-02-10)](https://pypi.org/project/ktpanda-modules/1.0.41/)
=============================

* Add dictutils ([524fd09](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/524fd09c091b7601983358fb412c7e26d885fedf))


[Version 1.0.40 (2024-11-20)](https://pypi.org/project/ktpanda-modules/1.0.40/)
=============================

* Add ttyutil.AsyncTTYInput ([ed192d4](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/ed192d46ed0e7f43c08991e412a96c1095ee1ab1))


[Version 1.0.39 (2024-10-31)](https://pypi.org/project/ktpanda-modules/1.0.39/)
=============================

* git_helper: Fix bug in get_changed_files ([7ea0978](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/7ea0978ad4919d18a59120a45a12dc322fed0b8d))


[Version 1.0.38 (2024-10-31)](https://pypi.org/project/ktpanda-modules/1.0.38/)
=============================

* Add get_config to git_helper.Git ([2f8572a](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/2f8572a3703f879b477432257e70d70702867bdd))


[Version 1.0.37 (2024-10-14)](https://pypi.org/project/ktpanda-modules/1.0.37/)
=============================

* Add dateutils.getmtime and allow users of TTYInput to specify time function ([b387c3c](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/b387c3c9f985977146fdebf8ddf2d22b7035d835))
* Add fileno() to TTYInput ([272905c](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/272905c64057cc63b90d0e97fab8351e633c6368))


[Version 1.0.36 (2024-07-14)](https://pypi.org/project/ktpanda-modules/1.0.36/)
=============================

* git_helper: Add status() ([fa6b111](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/fa6b11177690fa082ae66a5375714932e0c75784))


[Version 1.0.35 (2024-07-03)](https://pypi.org/project/ktpanda-modules/1.0.35/)
=============================

* Add git_helper ([6a6dc6a](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/6a6dc6ad9f6cc30b5924d15fdcdb4a3d78650c43))


[Version 1.0.34 (2024-05-21)](https://pypi.org/project/ktpanda-modules/1.0.34/)
=============================

* Add `create_dir` parameter to save_file and save_json ([5b8d5bb](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/5b8d5bb66640949af64f4e371af2becd7aab069f))


[Version 1.0.33 (2024-04-12)](https://pypi.org/project/ktpanda-modules/1.0.33/)
=============================

* Add roundtrip_encoding module ([3714fc8](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/3714fc81d74a7d10c94a62620d6d5186bc8d06e1))


[Version 1.0.32 (2024-04-11)](https://pypi.org/project/ktpanda-modules/1.0.32/)
=============================

* Add xmledit module ([413d0e8](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/413d0e8e0feb68fe8e2d1e0e39583193fd9f1c73))


[Version 1.0.31 (2024-01-01)](https://pypi.org/project/ktpanda-modules/1.0.31/)
=============================

* fileutils.py: Fix issue with Python 3.9 support ([c114b2f](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/c114b2f4701e21b3b45a3d2db56b5663b15c1f06))


[Version 1.0.30 (2023-06-28)](https://pypi.org/project/ktpanda-modules/1.0.30/)
=============================

* Fix some pylint errors ([bf4dc2d](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/bf4dc2db2ca624b26643e2f041eb2b2241f1002f))
* sqlite_helper: Add Query class ([5df49ff](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/5df49ffef86e703ffaa056fc5902850e4a051213))


[Version 1.0.29 (2023-06-10)](https://pypi.org/project/ktpanda-modules/1.0.29/)
=============================

* sqlite_helper: Make SQLiteDB.connect() and .close() idempotent ([4f15868](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/4f1586876392ee276c014ff46a43ad21f13bf434))


[Version 1.0.28 (2022-11-13)](https://pypi.org/project/ktpanda-modules/1.0.28/)
=============================

* Add cli.py module ([1083657](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/108365780263232e9f42003fd0c3403d02dd1ac1))


[Version 1.0.27 (2022-11-12)](https://pypi.org/project/ktpanda-modules/1.0.27/)
=============================

* vt100: Add KEYMAP ([d319014](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/d319014af0e74fbae690403dcea28b0f790d680e))


[Version 1.0.26 (2022-11-04)](https://pypi.org/project/ktpanda-modules/1.0.26/)
=============================

* ttyutil: Fix bug when `endtime` was not specified in `TTYInput`.readkey ([791b7a5](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/791b7a500fe4d2ef4615c62aa0f29938a753a430))
* ttyutil: Fix regex for detecting VT100 sequences ([3050887](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/305088708736956cd393754a66271d631b752fb2))
* ttyutil: Add `altkeypad` parameter to TTYInput ([a688903](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/a688903b7b352ad0fdb44cb601f4637281669c86))
* vt100: Add more codes for toggles ([980ccb5](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/980ccb5409e71521fa068fa39a4e6a533918dc7c))
* ttyutil: Refactor key parsing in TTYInput ([d03981e](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/d03981eb7dd6240aed5e21e3eb5fdd0bb3addb36))


[Version 1.0.25 (2022-11-01)](https://pypi.org/project/ktpanda-modules/1.0.25/)
=============================

* textcolor: Add Colorizer methods to wrap textattr functions ([f1476e6](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/f1476e613c7b5e1f87947c03b24ac00e693a07e8))
* ttyutil: Disable IXON when `signals` is False ([5fc9fc5](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/5fc9fc5a4a7778d8666d42f791fdc1eb41e4a29f))
* ttyutil: Fix bug with `rawbuf` not being cleared ([02a0787](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/02a078776be245147662742ef854c29f8466145d))


[Version 1.0.24 (2022-10-31)](https://pypi.org/project/ktpanda-modules/1.0.24/)
=============================

* Refactor TTYInput key reading ([9a44f13](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/9a44f13a0f774d31bbb3d5f2bcd66d4d77948311))
* Add fileutils module ([e3d1faa](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/e3d1faa49ca5da57481bf68e2dcea14c915ad769))


[Version 1.0.23 (2022-10-26)](https://pypi.org/project/ktpanda-modules/1.0.23/)
=============================

* ttyutil: Add `hidecurs` and `signal` to TTYInput ([f3caa75](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/f3caa75aa8e35440e9023ea41c7d79b82df1b3b5))
* dateutils: Add `split_time` function ([975c51e](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/975c51e7a5f80584e7a37f713e1e1272a2bebffc))


[Version 1.0.22 (2022-10-25)](https://pypi.org/project/ktpanda-modules/1.0.22/)
=============================

* Add missing MANIFEST.in, gendoc, and README.html ([fcac89a](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/fcac89a85ee8b72628f058b9408ed5533245d19b))
* Add ttyutil module ([8be01ca](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/8be01caf3e8008790b7a111d34865514f0a4e37d))
* Add vt100 module and update textcolor to use it ([eac462d](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/eac462db43c9f4b61cd68dc4e01f91ce48d33fc3))
* dateutils: Add prettytime ([8b356f3](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/8b356f3daee1ce534662dda392619162138f6f4a))
* Fix errors and warnings identified by pylint ([9c8053e](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/9c8053e4ddface194b93e22fb5d159680080750d))


[Version 1.0.21 (2022-10-20)](https://pypi.org/project/ktpanda-modules/1.0.21/)
=============================

* Fix parse_duration ([3bfc0e4](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/3bfc0e4dbdf0b1ed0ff64399f057257e6a9b15fd))


[Version 1.0.20 (2022-10-20)](https://pypi.org/project/ktpanda-modules/1.0.20/)
=============================

* Add dateutils module ([ba50dc7](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/ba50dc78c06a73c95b4378fbb67b068daed9de10))


[Version 1.0.19 (2022-10-06)](https://pypi.org/project/ktpanda-modules/1.0.19/)
=============================

* Update README.md to include `textcolor` ([96d12f1](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/96d12f115ae1393e3bd8062106a6d3f316dc8c04))
* Remove empty versioning.py ([8e84ca5](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/8e84ca50b95eff2a5907ef4fe99e63b98ac60b1f))
* Fix URL in setup.cfg ([c50c70d](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/c50c70d01594874da9e8e7dda1285e5e92bbd414))


[Version 1.0.18 (2022-10-02)](https://pypi.org/project/ktpanda-modules/1.0.18/)
=============================

* Add ability to do regex replace in alter_schema ([f055da8](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/f055da808a3ceafea07083701418862ecd3d91c4))


[Version 1.0.17 (2022-08-28)](https://pypi.org/project/ktpanda-modules/1.0.17/)
=============================

* Remove ktpanda.versioning; replaced with its own project 'versionator' ([153d775](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/153d7755a62f266e0b0397068c057b255fb2c547))
* Convert to using setup.cfg ([5904c17](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/5904c177d594594632d259466bbbecc0dbd0df07))


[Version 1.0.16 (2022-08-27)](https://pypi.org/project/ktpanda_modules/1.0.16/)
=============================

* Add CHANGELOG.md ([b9595e8](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/b9595e80d5a5cf40d4ad7cd23fcf8605c396d019))
* Add versioning module ([e513cfe](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/e513cfefbc6a4decad6340428fe08bd18ee74111))


[Version 1.0.15 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.15/)
=============================

* sqlite_helper: Fix bug with @in_transaction(attr='...') ([a79ce28](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/a79ce281f030ceed112138c9dc5c472a3022c4ae))


[Version 1.0.14 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.14/)
=============================

* sqlite_helper: Add type hints ([fc9970b](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/fc9970b60fe0acb99653aed2fdd5b487697eb6f1))


[Version 1.0.13 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.13/)
=============================

* textcolor: Add lpad and rpad ([d2a9f91](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/d2a9f91047efacc7da5f9694efbded2bba2bba2b))


[Version 1.0.12 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.12/)
=============================

* textcolor: Add Colorizer class ([270d640](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/270d640b31b1988dd439dcf7c9b9ea68d6ecdd6e))


[Version 1.0.11 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.11/)
=============================

* sqlite_helper: Add `attr` to in_transaction ([88908db](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/88908db6f01db9a0262645d36488ab50c9bcbcca))


[Version 1.0.10 (2022-08-26)](https://pypi.org/project/ktpanda-modules/1.0.10/)
=============================

* Remove __init__.py to make 'ktpanda' a namespace module ([5c4959a](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/5c4959a0c2eebf4392f158d48f7c12aaed876aad))


[Version 1.0.9 (2022-08-25)](https://pypi.org/project/ktpanda-modules/1.0.9/)
============================

* Add textcolor ([c79c02c](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/c79c02c466030425604c4a2571a5b740ea42ce53))
* Update author name in module headers ([a772d21](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/a772d215e686a0996bca152fed60cd83df536059))


[Version 1.0.8 (2022-08-24)](https://pypi.org/project/ktpanda-modules/1.0.8/)
============================

* Add an actual cursor subclass instead of using contextlib ([aff42aa](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/aff42aaddb20e8ccce6002944695a3ca4167a5cb))


[Version 1.0.7 (2022-08-24)](https://pypi.org/project/ktpanda-modules/1.0.7/)
============================

* threadpool: Fix missing 'sys' import ([96f889f](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/96f889fb86c31434852ef34be79633d5ad10c401))
* Refactor sqlite_helper and rename some methods ([c3d80d0](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/c3d80d09a76cd3c881bc2736c9d09487c9fcb587))
  * This makes things clearer but also breaks things. Currently I'm the only one using it, though.


[Version 1.0.6 (2022-08-18)](https://pypi.org/project/ktpanda-modules/1.0.6/)
============================

* Fix error when trying to roll back after failure ([2c9e247](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/2c9e247e184aae8cd0a242da35b0408ca705e257))


[Version 1.0.5 (2022-08-15)](https://pypi.org/project/ktpanda-modules/1.0.5/)
============================

* sqlite_helper: Add support for arbitrary pragmas ([8e44109](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/8e4410932321abb56e53681573d30e33faf35e05))


[Version 1.0.4 (2022-08-15)](https://pypi.org/project/ktpanda-modules/1.0.4/)
============================

* sqlite_helper.py: Add ability to specify transaction mode with `in_transaction` ([ff07da4](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/ff07da4c1a7bca7d462bf0ec4150a67d43755776))
* threadpool.py: Add ability to override default thread pool exception handler ([b47dbd8](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/b47dbd8b5a417add80845a70da130aa67794592f))
* Minor style fixes in docstrings ([31d3876](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/31d38767f650cf42f7f4dfa44f526dfca21fac49))


[Version 1.0.3 (2022-08-14)](https://pypi.org/project/ktpanda-modules/1.0.3/)
============================

* Add build requirement for setuptools ([7fc72c1](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/7fc72c175dae7fb97bd89553133dfa66575824e5))


[Version 1.0.2 (2022-08-14)](https://pypi.org/project/ktpanda-modules/1.0.2/)
============================

* Add README.md ([a68ce4d](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/a68ce4d1e01eb96b9afe88155b27459576f71d88))
* Initial commit ([18b4acd](https://gitlab.com/ktpanda/ktpanda-modules/-/commit/18b4acd67a71340bd029b3dbf74352059544085a))
